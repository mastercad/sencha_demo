Erste Schritte:
npm config set registry https://sencha.myget.org/F/community/npm/
npm login --registry=https://sencha.myget.org/F/community/npm/ --scppe=@sencha
Username: andreas.kempe..byte-artist.de (statt wie angegeben die wirkliche email)
Password:
Email:
Logged in as andreas.kempe..byte-artist.de on https://sencha.myget.org/F/community/npm/.

npm install -g @sencha/ext-gen

// alternativer weg
Sencha SDK herunter laden:

unter ~/sencha-sdks/6.7.0.63 entpacken

Sencha CMD herunter laden:

ausführen
SDK ordner wählen (~/sencha-sdks/)


Fehler:

Beim Installieren ALLER pakete kam ständig:

Not Found - GET https://sencha.myget.org/F/community/npm/fs-extra

Lösung:

npm set registry https://registry.npmjs.org/

Wichtige Links:

Definitionen der jeweiligen Klassen
http://docs.sencha.com/extjs/6.7.0/guides/quick_start/Set_Up_Start_Coding_the_App/Anatomy/Basic_Definitions_and_Vocabulary.html

